![Build Status](https://gitlab.com/Jcc10/GrrlPowerSkilltree/badges/master/build.svg)

---

This is a interactive version of Halo's (From the [Grrl Power Comic](http://grrlpowercomic.com/)) skill tree. It includes any information and also my personal thoughts on the skill tree. Mostly Wild-Mass-Guessing, but it does help.

View it [HERE](https://jcc10.gitlab.io/GrrlPowerSkilltree/)!

---

# Skill Tree Map

The skill tree map is currently the only feature of this project.

## Node Metadata

Most nodes contain metadata related to the node, this is shown to the right or below the map of nodes.

### ID
The ID of the node.
#### label
If the Label will cause the ID to display as [Label (ID)]
#### name
If the data.name is set it will cause the ID to display as [name (ID)]

### Status
The status just states if it can be unlocked (or is already unlocked) (Can be overwritten per node with data.status)

### Purchased
This is to state when stuff is unlocked so if you are looking for it you can find it.
#### link
If link is set the Purchase text will link to 'http://grrlpowercomic.com/archives/[link]' (If not set, it will just be text)
#### text
The text of the link (should be the comic #)

### Function
This is a array that becomes a un-ordered list of confirmed functionality. If the functionality has no confirmed node, it should be listed on the orb (If it does, it should be listed on the node that has the confirmed function)

### Thoughts
My personal thoughts or hypothesis on a nodes functionality.

## General Notes
Below the map are general notes on everything.
