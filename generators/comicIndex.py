# This generates a list of all comic pages
#
# This does not generate stuff for no non-standard comics
#   (Guest comics, pinups, etc.)

from lxml import html
import requests
import re
import json

# this sets the regex statements.
# [2] is the comic number, [4] is the comic name
GetComicNumber = re.compile(r"((Grrl Power #)(\d+)( – )(.{0,}))")
# [2] is the comic id
validHref = re.compile(r"(https?://(www.)?grrlpowercomic.com/archives/(\d+))")

# The index page URL.
url = "http://grrlpowercomic.com/archives"

# show that it is running
print('Downloading Index Data')

# download the pages
page = requests.get(url)
tree = html.fromstring(page.content)

# make the blank dict.
autoComics = {}

# Link path is long...
linkPath = "//div[contains(@class, 'post-page')]"
linkPath = linkPath + "/div[contains(@class, 'post-content')]/table//a"
# this gets a list of links
links = tree.xpath(linkPath)

# this ensures there actually are links
if(len(links) != 0):
    # ittarate over the links
    for link in links:
        # where does the link go?
        href = link.attrib["href"]
        # is it a comic link
        hrefMatch = validHref.match(href)
        # if a comic link is found
        if hrefMatch is not None:
            # get the text
            text = link.text_content()
            # does it match the regex of a comic link?
            textMatch = GetComicNumber.match(text)
            # if it does
            if textMatch is not None:
                # add it to the list
                autoComics[textMatch.groups()[2]] = {
                    "number": textMatch.groups()[2],
                    "title": textMatch.groups()[4],
                    "id": hrefMatch.groups()[2]
                }

# save to the data folder
with open('./../public/data/autoComicSet.json', 'w') as outfile:
    # sort the keys and have them indent so Git actually does diffs correctly
    json.dump(autoComics, outfile, sort_keys=True, indent=4)

# show it is done
print('Done!')
