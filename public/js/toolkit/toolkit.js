/* jshint esversion:6 */

/* "I'll bould my own jQuery! Without bloat, and ES6 Compatable!" */

const nothing = "This Value Is Nothing, It Never Exsisted, It Never Will Exsist";

class toolkit{
    constructor(cssSearch) {
      if (cssSearch !== null){
        this.elements = document.querySelectorAll(cssSearch);
        this.ele = true;
      }
    }

    doEach(f) {
      if(this.ele) {
        this.elements.forEach(f);
      } else {
        throw "No elements selector active!";
      }
    }

    replaceText(text) {
      this.doEach( function (element, index) {
          element.textContent = text;
        });
    }

    replaceHTML(html) {
      this.doEach( function (element, index) {
          element.innerHTML = html;
        });
    }

    changeStyle(property, value) {
        value = value || "";
        this.doEach( function (element, index) {
          element.style[property] = value;
        });
    }

    addListener(eventType, funcToDo){
      this.doEach( function (element, index) {
        element.addEventListener(eventType, funcToDo);
      });
    }

    value(value){
      if (this.ele) {
        value = value || nothing;
        if (value === nothing) {
          var values = [];
          this.doEach(function (element, index) {
            values.push(element.value);
          });
          if (values.length == 1) {
            return values[0];
          } else {
            return values;
          }
        } else {
          this.doEach(function (element, index) {
            element.value = value;
          });
        }
      }
    }

    setValididy(message) {
      if(this.ele){
        this.doEach(function (element, index) {
          element.setCustomValidity(message);
        })
      }
    }

    tellInvalid(message) {
      if(this.ele){
        this.doEach(function (element, index) {
          element.reportValidity();
        })
      }
    }

    toggleEnabled(status) {
      if(this.ele){
        status = status || "toggle";
        switch (status.toLowerCase()) {
          case "toggle":
            this.elements.forEach( function (element, index) {
              element.disabled = s;
            });
            break;
          case "on":
            this.elements.forEach( function (element, index) {
              element.disabled = false;
            });
            break;
          case "off":
            this.elements.forEach( function (element, index) {
              element.disabled = s;
            });
            break;
        }
      }
    }

    getUrlParam(param) {
      return this.getUrlParams()[param];
    }

    getUrlParams() {
      var url_string = window.location.href;
      var url = new URL(url_string);
      var params = {};
      for(var pair of url.searchParams.entries()) {
         params[pair[0]] = pair[1];
      }
      this.params = params;
      return params;
    }

    setParams(params) {
      var fileName = (window.location.href.split("/").slice(-1)) + "";
      fileName = fileName.split('?')[0]
      this.updatePageURL(fileName + "?" + params);
    }

    updatePageURL(url) {
      window.history.replaceState(null, null, url);
    }

    getJSON(url, f, params) {
      params = params || [];
      var xmlhttp = new XMLHttpRequest();
      var finalQ = "";
      url = "./testdata/" + url;
      if(params.length > 0) {
        var paramText = "";
        params.forEach(function (element, index) {
          paramText += element + "&";
        });
        finalQ = url + "?" + paramText;
      } else {
        finalQ = url;
      }
      xmlhttp.open('GET', finalQ, true);
      xmlhttp.onreadystatechange = function() {
          if (xmlhttp.readyState == 4) {
              if(xmlhttp.status == 200) {
                  var data = JSON.parse(xmlhttp.responseText);
                  f(data);
               }
          }
      };
      xmlhttp.send(null);
    }
}

export function tk(pass1) {
  return new toolkit(pass1);
}
