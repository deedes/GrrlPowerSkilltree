/* esversion: 6 */

// the toolkit for messing with HTML
import {tk} from "./../toolkit/toolkit.js";

// the text prosessor for JSON text
import {jsonTextProsessor} from './../toolkit/jsonTextMark.js';

/* <<<<< DATASET >>>>> */

// the dataset
import {orbDataset} from "./orbDataset/importMe.js";

// the metadata
import {absoluteDefaultMetadata, allGroupDefaultMetadata, generalSettings} from "./orbMapSettings.js";

/* <<<<< DATASET >>>>> */

/* end imports */

// TODO: make internal only functions have `__` before them

// declare `comicPointers` as a global var.
var comicPointers = "loading";


// TODO: replace this with a function in the toolkit, after I fix the toolkit... or not.
// this will run when the script is loaded.
{
  // create the request
  var xmlhttp = new XMLHttpRequest();
  // the url of the query
  var finalQ = "./data/autoComicSet.json";
  // build the query
  xmlhttp.open('GET', finalQ, true);
  // when we get the content
  xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4) {
          if(xmlhttp.status == 200) {
              // save it in the `comicPointers` var
              comicPointers = JSON.parse(xmlhttp.responseText);
              console.log("pointers downloaded");
           }
      }
  };

  // send the request
  xmlhttp.send(null);
}


/* `modLinks` function */
// modifies links so we only need to enter in a page number to get the comic number.
function modLinks(comicNumber) {

  // get the pointer object
  let pointer = comicPointers[comicNumber]

  // prep a empty text var for future use
  let text = "";

  // check to see if we have the pointers yet.
  if(comicPointers == "loading"){
    // this will click the submit button.
    let reloadText = 'ev = new Event("submit"); document.getElementById("searchForm").dispatchEvent(ev);';
    // if we don't, give a error.
    text = "<a onclick='" + reloadText + "' title='Click to load.'>Click to fix</a>";
  } else {
    // if it has, run normally.
    if(pointer){
      // if we found the pointer make the tag
      pointer.url = "http://grrlpowercomic.com/archives/" + pointer.id;
      pointer.page = "#" + pointer.number;
      pointer.hover = pointer.page + " - " + pointer.title;
      text = "<a href='" + pointer.url + "' title='" + pointer.hover + "'>" + pointer.page + "</a>";
    } else {
      // if we did not, return a error
      text = "<a title='POINTER NOT FOUND'>#" + comicNumber + "</a>";
    }
  }

  // return the tag.
  return text;
}
// end `modLinks` function


// initialise the text prosessor *after* the `modLinks` function so we can use it
// this is used for all text prosessing.
var jtp = jsonTextProsessor({linkStart: "http://grrlpowercomic.com/archives/", linkModderFunction: modLinks});


/* end prep work, begin routine work */





// Find the content of a node.
// returns undefined if no node is found.
export function findNode(searchID) {
  // this returns the object not the index. (thank god.)
  return orbDataset.nodes.find(function(node) {
      // if it's the node, return true, else false.
      return (node.id == searchID);
  });
}


/* start `flagDecorations` function */
// this creates the dots for the flag.
function flagDecorations(data){
  // blank dots text
  var flags = " ";

  // if a node is unlocked
  if (data.nodeUnlockedBasic) {
    // if it has a link
    if (data.nodeUnlockedLink) {
      // will this ever run anymore?
      // show the `unlockWithLink` flag symbol
      flags += "<span title='Unlock Time W Link'>" + generalSettings.symbols.flag.unlockWithLink + "</span> ";
    } else {
      // otherwise show the `unlockWithoutLink` symbol
      flags += "<span title='Unlock Time W/O Link'>" + generalSettings.symbols.flag.unlockWithoutLink + "</span> ";
    }
  }

  // if we know what it does
  if (data.nodeWhatItDoes) {
    // show the `whatItDoes` symobol
    flags += "<span title='Known Function'>" + generalSettings.symbols.flag.whatItDoes + "</span> ";
  }

  // and if we have notes
  if (data.nodeNote) {
    // show that it has notes.
    flags += "<span title='Has Theories'>" + generalSettings.symbols.flag.hasNotes + "</span> ";
  }

  // remove the trailing whitespace
  flags = flags.slice(0, -1);

  // return the decorations
  return flags;
}
/* end `flagDecorations` function */



/* start `runTextProsessor` function */
// it runs the text prosessor when the variable is not null... what more are you expecting
function runTextProsessor(data) {
  if (data !== undefined) {
    // if there is data run the prosessor.
    return jtp.prosessText(data);
  } else {
    return null;
  }
}
/* end `runTextProsessor` function */



/* start `setWindowText` function */
// sets the orb data pane's text.
function setWindowText(name, color, flag, status, unlock, whatItDoes, note, param) {

  // set the name
  tk("#NID").replaceHTML(name);

  // set the flag color
  tk("#NFlag").changeStyle("background", color);

  // set the flag decorations
  tk("#NFlag").replaceHTML(flag);

  // set the status
  tk("#NStatus").replaceHTML(status);

  // set the unlock date
  tk("#NObtain").replaceHTML(unlock);

  // set what it does
  tk("#NFunction").replaceHTML(whatItDoes);

  // set the note
  tk("#NHypothesis").replaceHTML(note);

  //if param, add it to the url bar (or wipe them all)
  if(param){
    tk().setParams('node=' + param);
  } else {
    tk().setParams('');
  }
}
/* end `setWindowText` function */



/* start `nameGenerator` function */
function nameGenerator(clickedNode, metadata) {
  let tempText = metadata.namePatern;
  let tempID = clickedNode.id;
  if(metadata.beautifulID){
    tempID = clickedNode.data.beautifulID;
  }
  tempText = tempText.replace("|id|", tempID);
  tempText = tempText.replace("|label|", clickedNode.label);
  return tempText.replace("|name|", metadata.name);
}
/* end `nameGenerator` function */



/* start `mapClick` function */
/* this runs when the map is clicked */
export function mapClick(properties) {

    // the id for the node
    var ids = properties.nodes;
    if (ids.length >= 1 ){
      //if there is a ID
        // if there is only one node
        if (ids.length == 1){
          // grab the node ID
          let searchID = ids[0];
          // then find the node and save it as `clickedNode`
          var clickedNode = findNode(searchID);
        }

        /* combine metadata until we get only one object */
        // default metadata is in OrbMetadata.js
        {
            // declare the combined objects *before* sending data to them.
            let groupDefaults = {}
            // the node's group default metadata
            if (allGroupDefaultMetadata[clickedNode.group] !== undefined){
              groupDefaults = allGroupDefaultMetadata[clickedNode.group];
            }

            // create the full default object as the fallthrough ({} =< absoluteDefaultMetadata < groupDefaults)
            // the first param is overwritten, so it needs to be a empty object.
            let defaultMD = Object.assign({}, absoluteDefaultMetadata, groupDefaults);


            let nodeMD = {};
            //ensure that nsMD is a valid object. (it *can* be left unset)
            if (clickedNode.data !== undefined) {
              // otherwise we get the data.
              nodeMD = clickedNode.data;
            }

            // now we make the final metadata object that is used almost excluseivly.
            var finalMD = Object.assign({}, defaultMD, nodeMD);
        }
        /* done combineing metadata */

        // run the dot's maker.
        var flagDots = flagDecorations(finalMD);

        // Name generator
        var finalName = nameGenerator(clickedNode, finalMD);

        // prosess the text that should be prosessed.
        finalMD.unlocked = runTextProsessor(finalMD.unlocked);
        finalMD.whatItDoes = runTextProsessor(finalMD.whatItDoes);
        finalMD.notes = runTextProsessor(finalMD.notes);

        // set the window text.
        setWindowText(finalName, finalMD.color.background, flagDots, finalMD.status, finalMD.unlocked, finalMD.whatItDoes, finalMD.notes, clickedNode.id);
    } else {
        // if there is not any id, set the text as the no-node data.
        let nn = generalSettings.noNode
        setWindowText(nn.name, nn.color, nn.flagDots, nn.status, nn.unlocked, nn.whatItDoes, nn.note, nn.param);
    }
}
/* end `mapClick` function */
