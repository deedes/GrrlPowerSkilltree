/* jshint esversion:6 */

/* This is for the defaults for the orbMap, other settings will be moved here in the future. */

// fallthrough default metadata:
export var absoluteDefaultMetadata = {
  namePatern: "|id|",
  status: "Data Missing",
  unlocked: {text:"Data Missing"},
  notes: "No current thoughts.",
  whatItDoes: "Unknown."
};

// the group metadata
export var allGroupDefaultMetadata = {
    "Purchased": {
      group: "Purchased",
      status: "Purchased",
      unlocked: "Unknown / Prior to Sydney Obtaining the Orbs.",
      color: {background:'#007F0E',border:'#073B0B'}
    },
    "Available": {
      group: "Available",
      status: "Unlocked, Awaiting Purchase.",
      unlocked: "It hasn't been Purchased yet!",
      color: {background:'#FE0000',border:'#800000'}
    },
    "UnAvailable": {
      group: "UnAvailable",
      status: "Locked",
      unlocked: "It can't be Purchased yet!",
      color: {background:'#FF00DC',border:'#4E0042'}
    },
    "On": {
      group: "On",
      status: "On",
      unlocked: "Unknown / Prior to Sydney Obtaining the Orbs.",
      color: {background:'#007F0E',border:'#073B0B'}
    },
    "Off": {
      group: "Off",
      status: "Locked",
      unlocked: "It has not been turned on!",
      color: {background:'#FE0000',border:'#800000'}
    },
    "Disabled": {
      group: "Disabled",
      status: "Disabled",
      unlocked: "It can't be turned on yet!",
      color: {background:'#FF00DC',border:'#4E0042'}
    },
    "VanishingPoint": {
      group: "VanishingPoint",
      status: "Presumed to exist however it's not shown",
      unlocked: "It's metadata!",
      color: {background:'gray',border:'darkgray'}
    },
    "Hidden": {
      group: "Hidden",
      status: "Used to display",
      unlocked: "It's metadata!",
      color: {background:'white',border:'white'}
    },
    "Orb": {
      group: "Orb",
      namePatern: "|label| (|id|)",
      status: "It's a orb!",
      unlocked: "It's a orb!",
      color: {background:'white',border:'white'}
    },
    "TrueCenter": {
      group: "TrueCenter",
      namePatern: "|name| (|id|)",
      status: "It's a weird one that requires multiple other unlocks...",
      unlocked: "2/7 were unlocked prior to prior to Sydney obtaining the orbs",
      color: {background:'linear-gradient(to right, #007f0e 25%,#fe0000 26%)',border:'white'}
    }
};

// General Program Settings
export var generalSettings = {
  symbols: {
    flag: {
      unlockWithLink: "📆",
      unlockWithoutLink: "📅",
      whatItDoes: "🔧",
      hasNotes: "💬"
    }
  },
  noNode: {
    name: "No Node Clicked",
    color: "white",
    flagDots: "",
    status: "Select a node to view.",
    unlocked: "Select a node to view.",
    whatItDoes: "Select a node to view.",
    note: "Select a node to view.",
    param: undefined
  }
};
