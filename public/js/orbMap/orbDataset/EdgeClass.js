/*jshint esversion: 6 */

class Edge {

  constructor(from, to, status, dashed, arrow, straight, length) {
    // set basic stuff
    this.from = from.toUpperCase();
    this.to = to.toUpperCase();
    this.width = 4

    if(status == "En"){
      this.color = {color:'#0125FF', highlight:'#0125FF', inherit:false};
    } else if(status == "Dis"){
      this.color = {color:'#3CC2E0', highlight:'#3CC2E0', inherit:false};
    } else if(status == "Invs"){
      this.color = {color:'silver', highlight:'silver', inherit:false};
      this.hidden = true;
    }

    if(dashed){
      // send Rainbow
      this.dashes = [24,8,8,8];
    }

    if(arrow){
      this.arrows = 'to';
    }

    if(straight){
      this.smooth = false;
    }

    if(length){
      if(length == "auto"){
      } else {
        this.length = length;
      }
    } else {
      this.length = 30;
    }
  }
}

export function edge(from, to, status, dashed, arrow, straight, length) {
  return (new Edge(from, to, status, dashed, arrow, straight, length));
}
