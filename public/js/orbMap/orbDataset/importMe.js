/* jshint esversion:6 */

// This file gethers all the files and passes them out as a single item.

import {comOrbNodes, comOrbEdges} from "./comOrb.js";
import {lightHookNodes, lightHookEdges} from "./lightHook.js";
import {PPONodes, PPOEdges} from "./PPO.js";
import {FLTNodes, FLTEdges} from "./FLT.js";
import {MBNodes, MBEdges} from "./MB.js";
import {UKNodes, UKEdges} from "./UK.js";
import {LSNodes, LSEdges} from "./LS.js";
import {outerCenterNodes, outerCenterEdges} from "./outerCenter.js";
import {innerCenterNodes, innerCenterEdges} from "./innerCenter.js";
import {nodeGroupStyles} from "./orbStyles.js";


var orbNodes = [...comOrbNodes, ...lightHookNodes, ...PPONodes, ...FLTNodes, ...MBNodes, ...UKNodes, ...LSNodes];
var orbEdges = [...comOrbEdges, ...lightHookEdges, ...PPOEdges, ...FLTEdges, ...MBEdges, ...UKEdges, ...LSEdges];


// this combines all the lists into a single list for the map to use.
// this is how we maintain easy to read code!
var allNodes = [...orbNodes, ...outerCenterNodes, ...innerCenterNodes];
var allEdges = [...orbEdges, ...outerCenterEdges, ...innerCenterEdges];

export var orbDataset = {nodes: allNodes, edges: allEdges, nodeStyles: nodeGroupStyles};
