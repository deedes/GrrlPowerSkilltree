/* jshint esversion:6 */
import {orb} from "./OrbClass.js";
// orb(ID, GROUP, X, Y, LABEL, IMAGE, DATA)
// any can be null except ID and GROUP.
import {edge} from "./EdgeClass.js";
// edge(from, to, status, dashed, arrow, straight, length)


//Inner Center Orbs
export var innerCenterNodes = [
    orb( 'Center.True', 'TrueCenter', 'zero', 'zero', null, null, {
      notes:"2 pie wedges enabled, most likely will become fully enabled if all connected nodes were enabled.",
      name:"True Center"
    }),
    orb( 'Center.CO', 'Off', 210, 45, null, null, {
      notes:"Most likely connected in some way to the Communications Orb"
    }, "off"),
    orb( 'Center.LH', 'Off', 165, -150, null, null, {
      notes:"Most likely connected in some way to the Light Hook Orb"
    }, "off"),
    orb( 'Center.PPO', 'Off', 'zero', -240, null, null, {
      notes:"Most likely connected in some way to the Weapons Orb"
    }, "off"),
    orb( 'Center.FLT', 'On', -165, -150, null, null, {
      notes:"Most likely connected in some way to the Propulsion Orb"
    }, "off"),
    orb( 'Center.MB', 'On', -210, 45, null, null, {
      notes:"Most likely connected in some way to the Shield Orb"
    }, "off"),
    orb( 'Center.UK', 'Off', -105, 180, null, null, {
      notes:"Most likely connected in some way to the 'Still Unknown' Orb"
    }, "off"),
    orb( 'Center.LS', 'Off', 105, 180, null, null, {
      notes:"Most likely connected in some way to the Life Support Orb"
    }, "off")
];
var innerCenterHidden = [
    edge('LH', 'Center.LH', 'Invs', false, false, true, 'auto'),
    edge('CO', 'Center.CO', 'Invs', false, false, true, 'auto'),
    edge('PPO', 'Center.PPO', 'Invs', false, false, true, 'auto'),
    edge('FLT', 'Center.FLT', 'Invs', false, false, true, 'auto'),
    edge('MB', 'Center.MB', 'Invs', false, false, true, 'auto'),
    edge('UK', 'Center.UK', 'Invs', false, false, true, 'auto'),
    edge('LS', 'Center.LS', 'Invs', false, false, true, 'auto')
];
var innerCenterVisible = [
    edge('Center.LH', 'Center.True', 'En', false, false, true, 'auto'),
    edge('Center.CO', 'Center.True', 'En', false, false, true, 'auto'),
    edge('Center.PPO', 'Center.True', 'En', false, false, true, 'auto'),
    edge('Center.FLT', 'Center.True', 'En', false, false, true, 'auto'),
    edge('Center.MB', 'Center.True', 'En', false, false, true, 'auto'),
    edge('Center.UK', 'Center.True', 'En', false, false, true, 'auto'),
    edge('Center.LS', 'Center.True', 'En', false, false, true, 'auto'),
    edge('Center.LH', 'Center.CO', 'Dis', true, false, false, 'auto'),
    edge('Center.PPO', 'Center.LH', 'Dis', true, false, false, 'auto'),
    edge('Center.FLT', 'Center.PPO', 'Dis', true, false, false, 'auto'),
    edge('Center.MB', 'Center.FLT', 'Dis', true, false, false, 'auto'),
    edge('Center.UK', 'Center.MB', 'Dis', true, false, false, 'auto'),
    edge('Center.LS', 'Center.UK', 'Dis', true, false, false, 'auto'),
    edge('Center.CO', 'Center.LS', 'Dis', true, false, false, 'auto')
  ];

// this combines the two lists into one.
export var innerCenterEdges = [...innerCenterHidden, ...innerCenterVisible];
