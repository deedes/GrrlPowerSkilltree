/*jshint esversion: 6 */

import { nodeGroupStyles } from "./orbStyles.js";

class Orb {

  constructor(id, group, x, y, label, image, data, commentBorder) {
    // set basic stuff
    this.id = id.toUpperCase();
    this.group = group;

    if(label) {
      this.label = label;
    }
    if(image){
      this.image = image;
    }

    var tempFixed = {}

    // Set X & Y
    if(x){
      this.x = x;
      if(x == 'zero'){
        this.x = 0;
      }
      tempFixed.x = true;
    }
    if(y){
      this.y = y;
      if(y == 'zero'){
        this.y = 0;
      }
      tempFixed.y = true;
    }
    // if X or Y, fix the point
    if((x)||(y)){
      this.fixed = tempFixed;
    }

    var localData = data || {};

    // data is also used if the id is not all caps, have to trigger in both sanarios.
    if ( (data) || (this.id !== id)){
      var tempData = data || {};
      if (tempData.notes){
        tempData.nodeNote = true;
      }
      if (tempData.unlocked){
        tempData.nodeUnlockedBasic = true;
      }
      if (tempData.ull){
        tempData.nodeUnlockedLink = true;
      }
      if (tempData.whatItDoes){
        tempData.nodeWhatItDoes = true;
      }
      if (this.id !== id){
        tempData.beautifulID = id;
      }
      this.colorModder(group, commentBorder, tempData);
      this.data = tempData;
    }
  }

  colorModder(group, commentBorder, data) {
      var bg = 'white';
      var bdr = 'white';
      var comment = 'gold';
      if (nodeGroupStyles[group].color) {
        if (nodeGroupStyles[group].color.background) {
          bg = nodeGroupStyles[group].color.background;
        }
        if (nodeGroupStyles[group].color.border) {
          bdr = nodeGroupStyles[group].color.border;
        }
      }
      var changed = false;
      var borderWidth = false;
      if(data.nodeUnlockedLink){
        bg = '#00CC84';
        comment = 'orange';
        changed = true;
      }
      if(data.nodeNote){
          if(commentBorder){
            if(commentBorder == "On"){
              bdr = comment;
              changed = true;
              borderWidth = true;
            } else {
              changed = false;
              borderWidth = false;
            }
          } else {
            var skipThese = ["Hidden", "Orb", "TrueCenter"]
            if(skipThese.indexOf(group) == -1){
              bdr = comment;
              changed = true;
              borderWidth = true;
            }
          }
      }
      if(changed){
        this.color = { border : bdr, background : bg};
      } else {
        delete this.color;
      }
      if(borderWidth){
        this.borderWidth = 2;
        this.size = 20;
      } else {
        delete this.borderWidth;
        delete this.size;
      }
  }
}

export function orb(id, group, x, y, label, image, data, commentBorder) {
  return (new Orb(id, group, x, y, label, image, data, commentBorder));
}
