/* jshint esversion:6 */
import {orb} from "./OrbClass.js";
// orb(ID, GROUP, X, Y, LABEL, IMAGE, DATA)
// any can be null except ID and GROUP.
import {edge} from "./EdgeClass.js";
// edge(from, to, status, dashed, arrow, straight, length)


// Flight Orb
export var FLTNodes = [
    orb( 'FLT', 'Orb', -520, -420, 'Propulsion Orb', './images/Orbs/PropulsionOrb.png', {
      status:"The Flight Orb",
      notes:"Why does the Com-Orb have a teleporter if this one provides Propulsion?<br />Does it provide a minor enviromental shield?",
      whatItDoes:{
        k:"ul",
        c:[
          "Flight up to Mk. 16.",
          "Removes fear of heights during use"
        ]
      }
    }),
    orb( 'FLT.A.1', 'Available', -700, -240, null, null, {
      notes:"Wormhole? Teleporter unlock looked simlar to this one."
    }),
    orb( 'FLT.B.1', 'Purchased', -780, -320, null, null, {
      notes:"This is the speed upgrade tree.",
      whatItDoes:{
        k:"ul",
        c:[
          "Base speed should be 12 Mph or 19 Km/h (5.4 m/s)",
          "This one should provide speed up to 47 Mph or 77 Km/h (21.4 m/s)"
        ]
      }
    }),
    orb( 'FLT.B.1.A.1', 'Purchased', -900, -320, null, null, {
      whatItDoes:"Should provide speed up to 191 Mph or 308 Km/h (85.75 m/s)"
    }),
    orb( 'FLT.B.1.A.2', 'Purchased', null, null, null, null, {
      whatItDoes:"Should provide speed up to Mk. 1. (343 m/s)"
    }),
    orb( 'FLT.B.1.A.3', 'Purchased', null, null, null, null, {
      whatItDoes:{
        k:"t",
        c:[
          "Provides speed up to Mk. 4. (1372 m/s)",
          {
            k: "sup",
            c: {
              k: "a",
              p: "670"
            }
          }
        ]
      }
    }),
    orb( 'FLT.B.1.A.4', 'Purchased', -1180, -560, null, null, {
      ull: true,
      unlocked: {
        k:"a",
        p:"660"
      },
      whatItDoes:{
        k:"t",
        c:[
          "Provides speed up to Mk. 16. (5488 m/s)",
          {
            k: "sup",
            c: {
              k: "a",
              p: "670"
            }
          }
        ]
      }
    }),
    orb( 'FLT.B.1.B.1', 'Available', -1120, -292, null, null, {
      whatItDoes: {
        k:'ul',
        c:[
          "This is the speed tree, so this might be FTL functionality.",
          {
            c: [
              "Appears to be abailable as of",
              {
                k:'a',
                p:'660'
              }
            ]
          }
        ]
      }
    }),
    orb( 'FLT.B.1.B.2', 'Available', null, null, null, null, {
      notes:{
        c:[
          "Appears to be unlocked as of",
          {
            k:'a',
            p:'660'
          }
        ]
      }
    }, "off"),
    orb( 'FLT.B.1.B.3', 'Available', null, null, null, null, {
      notes:{
        c:[
          "Appears to be unlocked as of",
          {
            k:'a',
            p:'660'
          }
        ]
      }
    }, "off"),
    orb( 'FLT.B.1.B.4', 'Available', null, null, null, null, {
      notes:{
        c:[
          "Appears to be unlocked as of",
          {
            k:'a',
            p:'660'
          }
        ]
      }
    }, "off"),
    orb( 'FLT.B.1.B.5', 'Available', -1360, -540, null, null, {
      notes:{
        c:[
          "Appears to be unlocked as of",
          {
            k:'a',
            p:'660'
          }
        ]
      }
    }, "off"),
    orb( 'FLT.B.1.B.5.VP', 'VanishingPoint', -1520, -480),
    orb( 'FLT.C.1', 'Purchased', -740, -360, null, null, {
      notes:"Turning radius? Inirtial Dampaners? How fast to go from 0 to Mk. 6?"
    }),
    orb( 'FLT.C.2', 'Purchased'),
    orb( 'FLT.C.3', 'Available'),
    orb( 'FLT.C.4', 'Available'),
    orb( 'FLT.C.5', 'Available', -1020, -600),
    orb( 'FLT.D.1', 'UnAvailable', -760, -520),
    orb( 'FLT.D.2', 'UnAvailable'),
    orb( 'FLT.D.3', 'UnAvailable'),
    orb( 'FLT.D.4', 'UnAvailable'),
    orb( 'FLT.D.4.VP', 'VanishingPoint', -760, -820),
    orb( 'FLT.E.VP', 'VanishingPoint', -620, -680)
];


export var FLTEdges = [
    edge('FLT', 'FLT.A.1', "En", true),
    edge('FLT', 'FLT.B.1', "En"),
    edge('FLT.B.1', 'FLT.B.1.A.1', "En"),
    edge('FLT.B.1.A.1', 'FLT.B.1.A.2', "En"),
    edge('FLT.B.1.A.2', 'FLT.B.1.A.3', "En"),
    edge('FLT.B.1.A.3', 'FLT.B.1.A.4', "En"),
    edge('FLT.B.1', 'FLT.B.1.B.1', "En"),
    edge('FLT.B.1.A.1', 'FLT.B.1.B.1', "En"),
    edge('FLT.B.1.A.2', 'FLT.B.1.B.1', "En"),
    edge('FLT.B.1.A.3', 'FLT.B.1.B.1', "En"),
    edge('FLT.B.1.A.4', 'FLT.B.1.B.1', "En"),
    edge('FLT.B.1.B.1', 'FLT.B.1.B.2', "En"),
    edge('FLT.B.1.B.2', 'FLT.B.1.B.3', "En"),
    edge('FLT.B.1.B.3', 'FLT.B.1.B.4', "En"),
    edge('FLT.B.1.B.4', 'FLT.B.1.B.5', "En", true),
    edge('FLT.B.1.B.5', 'FLT.B.1.B.5.VP', "Dis", false, true),
    edge('FLT', 'FLT.C.1', "En"),
    edge('FLT.C.1', 'FLT.C.2', "En"),
    edge('FLT.C.2', 'FLT.C.3', "En"),
    edge('FLT.C.3', 'FLT.C.4', "En"),
    edge('FLT.C.4', 'FLT.C.5', "En"),
    edge('FLT', 'FLT.D.1', "Dsl", true),
    edge('FLT.D.1', 'FLT.D.2', "Dis"),
    edge('FLT.D.2', 'FLT.D.3', "Dis"),
    edge('FLT.D.3', 'FLT.D.4', "Dis"),
    edge('FLT.D.4', 'FLT.D.4.VP', "Dis", false, true),
    edge('FLT', 'FLT.E.VP', "Dis", false, true)
];
