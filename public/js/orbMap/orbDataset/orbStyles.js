/* jshint esversion:6 */
// the group styles for the network

export var nodeGroupStyles = {
    UnAvailable: {
        color: {background:'#9300ff',border:'#750099'}
    },
    Available: {
        color: {background:'#FE0000',border:'#800000'}
    },
    Purchased: {
        color: {background:'#007F0E',border:'#073B0B'}
    },
    On: {
        color: {background:'#007F0E',border:'#073B0B'}
    },
    Off: {
        color: {background:'#FE0000',border:'#800000'}
    },
    Disabled: {
        color: {background:'#FF00DC',border:'#4E0042'}
    },
    VanishingPoint: {
        color: {background:'gray',border:'darkgray'}
    },
    Hidden: {
        color: {background:'white',border:'white'}
    },
    Orb: {
      shape: 'circularImage',
      size:40,
      font:{color:'black', size:'35', background:'#FFFFFF80'}
    },
    TrueCenter: {
      image:'./images/Orbs/PizzaPie.png',
      shape: 'image',
      color: {background:'#FFFFFF80',border:'#FFFFFF80'},
      shapeProperties: {
        useBorderWithImage:true
      },
      size:40
    }
};
