/* esversion: 6 */

// import the toolkit
import {tk} from "./../toolkit/toolkit.js";

// mapClick and findNode functions
import {mapClick, findNode} from './onNodeClick.js';

/* end imports */

// set network as a global function.
var network = null;

/* start `addSearch` function */
export function addSearch(passedNetwork) {
  // make the global the network function
  network = passedNetwork;

  // check the url to see if there is a node in it
  __checkURL();

  // set up the handle search event
  tk('#searchForm').addListener("submit", __onSearch);
  // end of search event

  // on search text change
  tk('#NIDS').addListener("change", __onTextChange);
  // end of search text change
}
/* end `addSearch` function */


/* start `__checkURL` function */
function __checkURL() {

  // grab the URL node.
  var nodeToFind = tk().getUrlParam('node');

  // if there is a node there
  if (nodeToFind !== undefined) {
    // TODO: make this into a function to reduce redundancy
    // make it upper case
    nodeToFind = nodeToFind.toUpperCase();

    // attempt to find the node in the dataset
    if (findNode(nodeToFind) !== undefined){
      // if it was found, run in a try-catch just in case
      try {
        // select the node on the map, but not the connected edges.
        network.selectNodes([nodeToFind], false);
        // set the search bar to the node ID
        tk('#NIDS').value(nodeToFind);
        // run the map-click function to display the node's data.
        mapClick({nodes:[nodeToFind]});

      } catch (e) {
        // sometimes throws error when it shouldn't, so we don't warn for bad URL's
      }
    } else {
      // if we were not looking for a empty string
      if(nodeToFind !== ""){
        // warn about a invalid node.
        tk('#NIDS').setValididy("Node Not Found.")
        tk('#NIDS').tellInvalid();
      }
    }
  }
}
/* end `__checkURL` function */


/* start `__onSearch` function */
function __onSearch(event) {
  // prevent actually submitting the search event.
  event.preventDefault();

  // get the node's id
  var nodeToFind = tk('#NIDS').value().toUpperCase();

  //see above for most comments
  if (nodeToFind !== undefined) {
    nodeToFind = nodeToFind.toUpperCase();
    if (findNode(nodeToFind) !== undefined){
      try {
        network.selectNodes([nodeToFind], false);
        mapClick({nodes:[nodeToFind]});
        tk('#NIDS').value(nodeToFind);
      } catch (e) {
        // if we have a error then it was probably the select nodes part,
        // display the node not found error.
        tk('#NIDS').setValididy("Node Not Found.")
        tk('#NIDS').tellInvalid();
      }
    } else {
      if(nodeToFind !== ""){
        tk('#NIDS').setValididy("Node Not Found.")
        tk('#NIDS').tellInvalid();
      }
    }
  }
}
/* end `__onSearch` function */



// Resets the valididy notification if the search text is changed.
function __onTextChange(event) {
  tk('#NIDS').setValididy("");
}
